#include "game.h"
#include <curses.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
  setlocale(LC_ALL, "en_US.UTF-8");
  struct game game;
  game_init(&game);
  while (game.running) {
    game_tick(&game);
  }
  game_free(&game);
}
