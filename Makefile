CC=gcc
CFLAGS=-g -Wall -Wextra -Wpedantic -fsanitize=leak,address,undefined
LIBS=-lncurses -lssl -lcrypto
LDFLAGS=
SRC = $(shell find . -type f -name '*.c')
HEADERS = $(shell find . -type f -name '*.h')
OBJ = $(SRC:%.c=%.o)

ttol: $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) $(LIBS) $(LDFLAGS) -o $@

%.o: %.c
	$(CC) $(CFLAGS) $? -c -o $@

.PHONY: clean format
clean:
	find . -type f -name '*.o' -exec rm {} +
	-rm ttol

format:
	clang-format -i $(SRC) $(HEADERS)
