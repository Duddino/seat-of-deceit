#ifndef GAME_H
#define GAME_H
#include "chat.h"
#include "info_box.h"
#include "input_box.h"
#include "network.h"
#include "window.h"
#include <curses.h>
#include <stdbool.h>

struct game {
  struct window windows[2];
  struct network network;
  bool running;
  size_t sel_win;
};

void game_init(struct game *self);
void game_free(struct game *self);
struct chat *game_get_chat(struct game *self);
void game_tick(struct game *self);

#endif
