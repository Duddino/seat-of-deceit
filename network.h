#ifndef NETWORK_H
#define NETWORK_H

#include "chat.h"
#include <openssl/ssl.h>

struct packet {
  union {
    struct message message;
  } data;
  enum { PKT_NONE, PKT_MSG } id;
};

void packet_free(struct packet *self);

struct network {
  SSL *ssl;
};

void network_init(struct network *self);

// Returns a packet if there is one pending.
// If there are no packets, pkt->id will be set to PKT_NONE
void network_receive(struct network *self, struct packet *pkt);

void network_free(struct network *self);

#endif
