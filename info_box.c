#include "info_box.h"
#include "common.h"
#include <stdlib.h>
#include <string.h>

void info_init(struct info_box *self, char *str, int col, int rows, int y,
               int x) {
  self->win = newwin(col, rows, y, x);
  self->scroll = 0;
  if (str != NULL) {
    size_t len = strlen(str);
    self->str = malloc(len + 1);
    EOR(self->str);
    strcpy(self->str, str);
  }
}

void info_tick(struct info_box *self) {
  wmove(self->win, 1, 1);
  int y = -self->scroll + 1, x = 1, max_y, max_x;
  getmaxyx(self->win, max_y, max_x);
  max_x--;
  max_y--;
  for (char *c = self->str; *c && y < max_y; c++) {
    if (*c == '\n' || x >= max_x) {
      x = 1;
      y++;
      if (*c != '\n')
        c--;
    } else {
      if (y > 0)
        mvwaddch(self->win, y, x, *c);
      x++;
    }
  }

  wrefresh(self->win);
}

void info_input(struct info_box *self, int c) {
  if (c == KEY_UP) {
    self->scroll++;
  } else if (c == KEY_DOWN) {
    self->scroll--;
  }
}

void info_free(struct info_box *self) {
  delwin(self->win);
  free(self->str);
}
