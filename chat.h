#ifndef CHAT_H
#define CHAT_H
#include "input_box.h"
#include <curses.h>

struct message {
  char *name;
  char *body;
};

void message_init(struct message *self, const char *name, const char *body);
size_t message_len(const struct message *self);
void message_to_str(const struct message *self, char *str);
void message_free(struct message *self);

struct chat {
  WINDOW *win;
  int max_x, max_y;
  int scroll;
  struct message *messages;
  size_t msg_size;
  struct input_box input;
};

void chat_init(struct chat *self, int cols, int rows, int y, int x,
               int input_cols, int input_y);
void chat_tick(struct chat *self);
void chat_add_msg(struct chat *self, struct message msg);
void chat_input(struct chat *self, int c);
void chat_free(struct chat *self);
#endif
