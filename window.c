#include "window.h"
#include <curses.h>

void window_box(struct window *self, bool highlight) {
  // The first parameter will always be a window.
  // Not sure if it's UB or not, but probably not
  WINDOW *win = *(WINDOW **)(self->win);
  werase(win);
  if (highlight) {
    attron(COLOR_PAIR(1));
    wborder(win, ACS_VLINE | COLOR_PAIR(1), ACS_VLINE | COLOR_PAIR(1),
            ACS_HLINE | COLOR_PAIR(1), ACS_HLINE | COLOR_PAIR(1),
            ACS_ULCORNER | COLOR_PAIR(1), ACS_URCORNER | COLOR_PAIR(1),
            ACS_LLCORNER | COLOR_PAIR(1), ACS_LRCORNER | COLOR_PAIR(1));
    attroff(COLOR_PAIR(1));
  } else {
    box(win, 0, 0);
  }
}
