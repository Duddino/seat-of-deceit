#include "game.h"
#include "chat.h"
#include "common.h"
#include "input_box.h"
#include "network.h"
#include "window.h"
#include <curses.h>
#include <stdlib.h>

char *str = "Class: King\n\n\
You are the king of this realm";

void game_init(struct game *self) {
  initscr();
  start_color();
  cbreak();
  timeout(0);
  keypad(stdscr, true);
  noecho();

  init_pair(1, COLOR_YELLOW, COLOR_BLACK);

  int max_y, max_x;
  getmaxyx(stdscr, max_y, max_x);

  struct chat *chat = malloc(sizeof(*chat));
  EOR(chat);
  chat_init(chat, max_y - 1, max_x / 2, 0, 0, max_x, max_y - 1);
  INIT_WINDOW(&self->windows[0], chat);

  struct info_box *class_description = malloc(sizeof(*class_description));
  EOR(class_description);
  info_init(class_description, str, max_y - 1, max_x / 2, 0, max_x / 2);
  INIT_WINDOW(&self->windows[1], class_description);

  self->running = true;
  self->sel_win = 0;

  refresh();
  for (size_t i = 0; i < LENGTH(self->windows); i++) {
    window_box(&self->windows[i], self->sel_win == i);
    WINDOW_CALL(&self->windows[i], tick, self->windows[i].win);
  }

  network_init(&self->network);
}

void game_tick(struct game *self) {
  int c = getch();
  struct packet pkt;
  network_receive(&self->network, &pkt);
  if (pkt.id == PKT_MSG) {
    // Can't think of a better way of passing the messages to the chat
    // Currently, the chat doesn't get ticked if it's not selected
    struct chat *chat = game_get_chat(self);
    chat_add_msg(chat, pkt.data.message);
  }
  if (c != ERR || pkt.id != PKT_NONE) {
    struct window *sel_win = &self->windows[self->sel_win];
    switch (c) {
    case ERR:
      break;
    case 'x':
      window_box(sel_win, false);
      WINDOW_CALL(sel_win, tick, sel_win->win);
      self->sel_win = (self->sel_win + 1) % LENGTH(self->windows);
      sel_win = &self->windows[self->sel_win];

      break;
    case KEY_F(1):
      self->running = false;
      break;
    default: {
      WINDOW_CALL(sel_win, input, sel_win->win, c);
    }
    }
    window_box(sel_win, true);
    WINDOW_CALL(sel_win, tick, sel_win->win);
  }
  napms(16);
}

struct chat *game_get_chat(struct game *self) {
  for (int i = 0; i < LENGTH(self->windows); i++) {
    if (self->windows[i].win_type == W_CHAT) {
      return self->windows[i].win;
    }
  }
  return NULL;
}

void game_free(struct game *self) {
  for (size_t i = 0; i < LENGTH(self->windows); i++) {
    WINDOW_CALL(&self->windows[i], free, self->windows[i].win);
    free(self->windows[i].win);
  }
  network_free(&self->network);

  endwin();
}
