#include "network.h"
#include "seat-of-deceit-network/network_common.h"
#include <openssl/ssl.h>
#include <unistd.h>

void packet_free(struct packet *self) {
  if (self->id == PKT_MSG) {
    message_free(&self->data.message);
  }
}

void network_init(struct network *self) {
  self->ssl = init_client_ssl(create_client_context(), 8080);
}

void network_receive(struct network *self, struct packet *pkt) {
  if (!self->ssl) {
    // Later we might want to try to reconnect to the server
    pkt->id = PKT_NONE;
    return;
  }
  char buff[500];
  int read = SSL_read(self->ssl, buff, sizeof(buff) - 1);
  if (read <= 0) {
    ssl_handle_errors(&self->ssl, read, 0);
    pkt->id = PKT_NONE;
    return;
  }
  pkt->id = PKT_MSG;

  buff[read] = '\0';
  message_init(&pkt->data.message, "Bob", buff);
}

void network_free(struct network *self) {
  if (self->ssl) {
    int fd = SSL_get_fd(self->ssl);
    SSL_free(self->ssl);
    close(fd);
  }
}
