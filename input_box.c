#include "input_box.h"
#include "common.h"
#include <ctype.h>
#include <curses.h>
#include <stdlib.h>
#include <string.h>

#define DEFAULT_CAPACITY 32
#define DEFAULT_GROW 16

void input_init(struct input_box *self, int col, int y) {
  self->win = newwin(1, col, y, 0);
  self->str = malloc(DEFAULT_CAPACITY);
  EOR(self->str);
  self->str_size = 0;
  self->str_capacity = DEFAULT_CAPACITY;
}

void input_tick(struct input_box *self) {
  werase(self->win);
  int max_x = getmaxx(self->win);
  int len = self->str_size;

  for (int i = len - 1, x = max_x - 1; i >= 0 && x >= 0; i--, x--) {
    mvwaddch(self->win, 0, len < max_x ? i : x, self->str[i]);
  }
  wmove(self->win, 0, MIN(max_x - 1, len));
  wrefresh(self->win);
}

void input_raw_push_char(struct input_box *self, int c) {
  self->str_size++;
  if (self->str_size > self->str_capacity) {
    self->str = realloc(self->str, self->str_capacity + DEFAULT_GROW);
    EOR(self->str);
    self->str_capacity += DEFAULT_GROW;
  }
  self->str[self->str_size - 1] = c;
}

void input_input(struct input_box *self, int c) {
  if (!iscntrl(c) && isascii(c)) {
    input_raw_push_char(self, c);
  }
  if (c == KEY_BACKSPACE)
    input_pop_char(self);
}

void input_pop_char(struct input_box *self) {
  if (self->str_size > 0) {
    self->str_size--;
  }
}

// Returns a string and clears the input_box.
// The pointer is valid as long as `input_free` or
// `input_push_char` are not called
void input_pop_str(struct input_box *self, char *str) {
  input_raw_push_char(self, '\0');
  memcpy(str, self->str, self->str_size);
  self->str_size = 0;
}

void input_free(struct input_box *self) {
  delwin(self->win);
  free(self->str);
}
