#ifndef WINDOW_H
#define WINDOW_H

#include "chat.h"
#include "info_box.h"
#include "input_box.h"
#include <assert.h>

#define INIT_WINDOW(ptr, w)                                                    \
  do {                                                                         \
    (ptr)->win = (w);                                                          \
    (ptr)->win_type = _Generic((w), struct chat *                              \
                               : W_CHAT, struct input_box *                    \
                               : W_INPUT, struct info_box *                    \
                               : W_INFO, default                               \
                               : assert(0));                                   \
  } while (0)

#define WINDOW_CALL(self, fn, ...)                                             \
  do {                                                                         \
    switch ((self)->win_type) {                                                \
    case W_CHAT:                                                               \
      chat_##fn(__VA_ARGS__);                                                  \
      break;                                                                   \
    case W_INPUT:                                                              \
      input_##fn(__VA_ARGS__);                                                 \
      break;                                                                   \
    case W_INFO:                                                               \
      info_##fn(__VA_ARGS__);                                                  \
    }                                                                          \
  } while (0)

struct window {
  void *win;
  enum { W_CHAT, W_INPUT, W_INFO } win_type;
};

void window_box(struct window *self, bool highlight);

#endif
