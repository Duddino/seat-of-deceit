#include "chat.h"
#include "common.h"
#include "input_box.h"
#include <curses.h>
#include <stdlib.h>
#include <string.h>

void message_init(struct message *self, const char *name, const char *body) {
  self->name = malloc(strlen(name) + 1);
  EOR(self->name);
  strcpy(self->name, name);
  self->body = malloc(strlen(body) + 1);
  EOR(self->body);
  strcpy(self->body, body);
}

size_t message_len(const struct message *self) {
  return strlen(self->name) + strlen(self->body) + 2; // 2 is colon and space
}

void message_to_str(const struct message *self, char *str) {
  sprintf(str, "%s: %s", self->name, self->body);
}

void message_free(struct message *self) {
  free(self->name);
  free(self->body);
}

void chat_init(struct chat *self, int cols, int rows, int y, int x,
               int input_cols, int input_y) {
  self->max_y = cols;
  self->max_x = rows;
  self->win = newwin(cols, rows, y, x);
  self->msg_size = 0;
  self->messages = NULL;
  self->scroll = 0;

  input_init(&self->input, input_cols, input_y);
}

void print_messages(struct chat *self) {
  int current_line = self->max_y - 2;

  size_t buffer_len = 100;
  char *buffer = malloc(buffer_len);
  EOR(buffer);

  // Let's start from the  last message and
  // Print each message keeping track of how many lines it needs
  for (int i = self->msg_size - 1 - self->scroll; i >= 0 && current_line >= 1;
       i--) {
    const struct message *msg = &self->messages[i];
    size_t msg_len = message_len(msg);

    if (msg_len + 1 > buffer_len) {
      buffer = realloc(buffer, msg_len + 1);
      buffer_len = msg_len + 1;
      EOR(buffer);
    }

    message_to_str(msg, buffer);
    while (current_line >= 1) {
      size_t starting_point = msg_len - (msg_len % (self->max_x - 2));
      if (starting_point == msg_len) {
        starting_point -= self->max_x - 2;
      }
      mvwprintw(self->win, current_line, 1, "%s", buffer + starting_point);
      buffer[starting_point] = 0;

      msg_len = starting_point;
      current_line--;
      if (starting_point < (size_t)self->max_x - 2)
        break;
    }
  }
  free(buffer);
}

void chat_add_msg(struct chat *self, struct message msg) {
  self->msg_size++;
  self->messages =
      realloc(self->messages, sizeof(*self->messages) * self->msg_size);
  EOR(self->messages);
  self->messages[self->msg_size - 1] = msg;
}

void chat_tick(struct chat *self) {
  print_messages(self);
  wrefresh(self->win);

  input_tick(&self->input);
}

void chat_input(struct chat *self, int c) {
  switch (c) {
  case KEY_UP:
    self->scroll++;
    break;
  case KEY_DOWN:
    self->scroll--;
    if (self->scroll < 0) {
      self->scroll = 0;
    }
    break;
  case '\n':
  case KEY_ENTER: {
    char *str = malloc(self->input.str_size + 1);
    EOR(str);
    input_pop_str(&self->input, str);
    char *name = malloc(4);
    EOR(name);
    strcpy(name, "You");
    chat_add_msg(self, (struct message){name, str});
    break;
  }
  case 8:
  case KEY_BACKSPACE:
    input_pop_char(&self->input);
    break;
  default:
    input_input(&self->input, c);
  }
}

void chat_free(struct chat *self) {
  for (size_t i = 0; i < self->msg_size; i++) {
    message_free(&self->messages[i]);
  }
  free(self->messages);
  input_free(&self->input);
  delwin(self->win);
}
