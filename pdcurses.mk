CC=gcc
CFLAGS=-g -Wall -Wextra -Wpedantic -fsanitize=leak,address,undefined -I "/usr/include/xcurses/"
LIBS=-lpdcurses -lSDL -lssl -lcrypto
LDFLAGS=
SRC = $(shell find . -type f -name '*.c')
OBJ = $(SRC:%.c=%.o)

ttol: $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) $(LIBS) $(LDFLAGS) -o $@

%.o: %.c
	$(CC) $(CFLAGS) $? -c -o $@

.PHONY: clean
clean:
	find . -type f -name '*.o' -exec rm {} +
	-rm ttol
