#ifndef INPUT_BOX
#define INPUT_BOX

#include <curses.h>

struct input_box {
  WINDOW *win;

  char *str;
  size_t str_size;
  size_t str_capacity;

  int max_x, max_y;
};

void input_init(struct input_box *self, int col, int y);
void input_tick(struct input_box *self);

// Fills str if it's not null and clears the input_box
// str must be at least `str_size+1` long
void input_pop_str(struct input_box *self, char *str);

void input_input(struct input_box *self, int c);
void input_pop_char(struct input_box *self);
void input_free(struct input_box *self);

#endif
