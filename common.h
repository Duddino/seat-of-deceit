#ifndef COMMON_H
#define COMMON_H

#define LENGTH(x) (sizeof((x)) / sizeof(*(x)))

#define EOR(x)                                                                 \
  do {                                                                         \
    if (!(x))                                                                  \
      (fprintf(stderr, "Variable `%s` in %s:%d is null", #x, __FILE__,         \
               __LINE__),                                                      \
       exit(1));                                                               \
  } while (0)

#define MAX(x, y) ((x) > (y) ? (x) : (y))
#define MIN(x, y) ((x) < (y) ? (x) : (y))

#endif
