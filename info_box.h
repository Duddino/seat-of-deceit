#ifndef INFO_BOX
#define INFO_BOX

#include <curses.h>

struct info_box {
  WINDOW *win;
  char *str;
  int scroll;
};

void info_init(struct info_box *self, char *str, int col, int rows, int y,
               int x);
void info_input(struct info_box *self, int c);
void info_tick(struct info_box *self);
void info_free(struct info_box *self);

#endif
